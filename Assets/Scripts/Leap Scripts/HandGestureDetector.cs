﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandGestureDetector : MonoBehaviour
{
    public CapsuleHand hand;

    Hand leapHand;
    Vector3 initialPinchPosition;

    private void Update()
    {
        leapHand = hand.GetLeapHand();
    }

    public Vector3 getFingerXYOffset(int fingerIndex)
    {
        if (leapHand == null || initialPinchPosition.magnitude == 0)
            return new Vector3();

        var diff = initialPinchPosition - leapHand.Fingers[fingerIndex].TipPosition.ToVector3();
        diff.z = 0;

        return diff;
    }

    public bool getPinchedFingers(int fingerIndex, float threshold)
    {
        if (leapHand == null)
            return false;

        var finger = leapHand.Fingers[fingerIndex].TipPosition;
        var thumb = leapHand.GetThumb().TipPosition;

        return thumb.DistanceTo(finger) < threshold;
    }

    public Vector3 getFingerPosition(int fingerIndex)
    {
        if (leapHand == null || initialPinchPosition.magnitude == 0)
            return new Vector3();

        return leapHand.Fingers[fingerIndex].TipPosition.ToVector3();
    }

    public void setInitialPinch()
    {
        if (leapHand == null)
            return;

        var thumb = leapHand.GetThumb().TipPosition;
        initialPinchPosition = thumb.ToVector3();
    }
}
